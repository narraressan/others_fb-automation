
const $ = require('cheerio')
const Nightmare = require("nightmare")
const nightmare = Nightmare({ show: true })

var username = "";
var password = "";

// you must be an admin of this group
// example: "https://www.facebook.com/groups/335153089912972/"
var group_url = "";

nightmare
	.goto("https://www.facebook.com/")
	.type("#email", username)
	.type("#pass", password)
	.click("#loginbutton > input")
	.wait("div[aria-label='Facebook']")
	.evaluate(() => {
		console.log("Successful Login: ");
	})
	.goto(group_url + "members/")
	.wait("#groupsMemberSection_all_members")
	.click("#groupsMemberSection_all_members > div > div > div > a")
	.click("ul[role='menu'] > li:nth-child(3) > a")
	.wait("#groupsMemberSection_all_members")
	.wait(1000)
	.evaluate(() => {
		var elements = document.querySelector("#groupsMemberSection_all_members").querySelectorAll(".fbProfileBrowserList.fbProfileBrowserListContainer > ul > div");
		var results = [];
		var logs = [];

		Array.from(elements).forEach((el) => {
			try{
				el.querySelector("div > div > div > a[role='button']").click();

				function tryMessage(){
					try{
						document.querySelector("div._1p1t").innerHTML = "";
						document.querySelector("div[contenteditable='true']").querySelector("br[data-text='true']").parentNode.innerHTML = "<span data-text='true'>Pre Defined Message...</span>";
						return true;
					}
					catch(err1){
						logs.push("LVL1-ERR");
						return false;
					}
				}

				// 10 retries ---> do not do recursion!
				for (var i = 0; i < 10; i++) { if(tryMessage() == true){ break; } }

				results.push(el.innerHTML);
			}
			catch(err2){ logs.push("LVL2-ERR"); }
		});

		return {
			"results": results,
			"logs": []
		};
	})
	// .end()
	.then((data) => {
		console.log(data["logs"]);

		var results = data["results"];
		for (var i = 0; i < results.length; i++) {
			console.log($(results[i]).text());
		}
	})
	.catch(error => { console.error("Automation Failed: ", error); })